<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('articles')->delete();
      
      Article::create([
        'id' => 1,
        'user_id' => 1,
        'title' => 'My first article',
        'body' => '1st article body Lorem Ipsum.',
        'published_at' => '2015-07-01']);
      
      Article::create([
        'id' => 2,
        'user_id' => 1,
        'title' => 'My Second article',
        'body' => '2nt article body Lorem Ipsum.',
        'published_at' => '2015-07-10']);
      
      Article::create([
        'id' => 3,
        'user_id' => 2,
        'title' => 'His first article',
        'body' => '1st article of Luciano body Lorem Ipsum.',
        'published_at' => '2015-07-20']);
      
      Article::create([
        'id' => 4,
        'user_id' => 1,
        'title' => 'My third article',
        'body' => '3rd article body Lorem Ipsum.',
        'published_at' => '2015-08-01']);
      
      Article::create([
        'id' => 6,
        'user_id' => 2,
        'title' => 'His second article',
        'body' => '2nd article of Luciano body Lorem Ipsum.',
        'published_at' => '2015-07-23']);
      
      Article::create([
        'id' => 7,
        'user_id' => 1,
        'title' => 'My fourth article',
        'body' => '4th article body Lorem Ipsum.',
        'published_at' => '2015-07-30']);
      
      Article::create([
        'id' => 8,
        'user_id' => 1,
        'title' => 'His third article',
        'body' => '3rd article of Luciano body Lorem Ipsum.',
        'published_at' => '2015-08-01']);
      
      Article::create([
        'id' => 9,
        'user_id' => 1,
        'title' => 'My fifth article',
        'body' => '5th article body Lorem Ipsum.',
        'published_at' => '2015-07-22']);
      
      Article::create([
        'id' => 10,
        'user_id' => 2,
        'title' => 'His fourth article',
        'body' => '4th article of Luciano body Lorem Ipsum.',
        'published_at' => '2015-07-22']);
    }
}
