<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->delete();
      
      User::create([
        'id' => 1,
        'name' => 'Mario Russo',
        'email' => 'mariorusso@gmail.com',
        'password' => bcrypt('mr1984'),]);
        
      User::create([
        'id' => 2,
        'name' => 'Luciano Russo',
        'email' => 'lrusso@gmail.com',
        'password' => bcrypt('lr1984'),]);
    }
}
