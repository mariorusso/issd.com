<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Article extends Model
{
  protected $fillable = array( 'title', 'excerpt', 'body', 'published_at' );
  
  protected $dates = array( 'published_at' );
  
  public function scopePublished($query)
  {
    $query->where('published_at', '<=', Carbon::now());
  }
  
  public function scopeRecent($query)
  {
    $query->published()->limit(3); 
  }
  
  public function setPublishedAtAttribute($date)
  {
    $this->attributes['published_at'] = Carbon::parse($date);
  }
  
  public function getPublishedAtAttribute($date)
  {
    return Carbon::parse($date)->format('Y-m-d');
  }
  
  
  public function user()
  {
     return $this->belongsTo('App\User');
  } 
    //
}
