<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
  public function index()
  {
  
    $title = 'WELCOME!!!';
    return view('pages.home', compact('title'));
  }
  
   public function about()
  {
    $title = 'About Page';
    $students = ['Mario', 'Chris', 'Yakub', 'Jose'];
    return view('pages.about', compact('title', 'students'));
  }
  
   public function support()
  {
    $title = 'Support Page';
   return view('pages.support', compact('title'));
  }
  
   public function contact()
  {
    $title = 'Contact Page';
    return view('pages.contact', compact('title'));
  }
   
   public function terms()
  {
    $title = 'Terms Page';
    $content = 'This is the terms view';
    return view('pages.default', compact('title', 'content'));
  }
  
  public function privacy()
  {
    $title = 'Privacy Page';
    $content = 'This is the Privacy view';
    return view('pages.default', compact('title', 'content'));
  }
  
  public function legal()
  {
    $title = 'Legal Page';
    $content = 'This is the Legal view';
    return view('pages.default', compact('title', 'content'));
  }
  
  
    //
}
