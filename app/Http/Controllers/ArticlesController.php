<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaveArticleRequest;
use Auth;
use App\Article;

class ArticlesController extends Controller
{
  
    public function __construct()
    {
      $this->middleware('auth', ['except' => ['show', 'index']]);
    }
  
    public function index()
    {       
      $title = 'Articles';
      $articles = \App\Article::latest()->published()->get();
      $recents = \App\Article::latest()->recent()->get();
      //$latest = \App\Article::latest()->published()->first();
      return view ('articles/article_list', compact('title', 'articles', 'latest', 'recents'));
    }
  
    public function show(Article $article)
    {
      $title = 'Articles';
      $recents = Article::recent();
      return view ('articles.article_single', compact('title', 'article', 'recents'));
    }
  
  
    public function create()
    {      
        $title = 'Create Article';
        return view ('articles/article_create', compact('title'));
    }
  
    public function store(SaveArticleRequest $request)
    {
      
      $article = Auth::user()->articles()->create($request->all());
      flash()->success('Article Created Successfuly');
      return redirect('articles'); 
    }
  
    public function edit(Article $article)
    {
      $title = 'Edit Articles';
      return view('articles/edit_article', compact('title', 'article'));
    }
  
    public function update(Article $article, SaveArticleRequest $request)
    {
      $article->update($request->all());
      flash()->success('Article Updated Successfuly');
      return redirect('articles');
    }
}
