<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/* PAGE ROUTES */
Route::get('/', 'PagesController@index');

Route::get('home', 'PagesController@index'); 

Route::get('about', 'PagesController@about');

Route::get('support', 'PagesController@support');

Route::get('contact', 'PagesController@contact');

Route::get('privacy', 'PagesController@privacy');

Route::get('legal', 'PagesController@legal');

Route::get('terms', 'PagesController@terms');



/* ARTICLES ROUTES */
/*Route::get('articles', 'ArticlesController@index');

Route::get('articles/create', 'ArticlesController@create');

Route::post('articles', 'ArticlesController@store');

Route::get('articles/{articles}', 'ArticlesController@show');

Route::get('articles/{articles}/edit', 'ArticlesController@edit');

Route::patch('articles/{articles}', 'ArticlesController@update');*/

/* AUTHENTICATION */

Route::resource('articles', 'ArticlesController');

Route::controllers(['auth' => 'Auth\AuthController',
                    'password' => 'Auth\PasswordController' ]);



