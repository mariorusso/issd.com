@extends('app')

@section('content')
<h1>{{ $title }}</h1>
      
{!! $content !!}
      
@stop('content')