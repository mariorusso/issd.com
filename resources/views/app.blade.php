<!DOCTYPE html>

<html>
  <head>
      <title>{{ $title }}</title>
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    
    <style type="text/css">
      #wrapper {
      	margin-top: 20px;
      }
    </style>
  </head>

  <body>
      @include('partials._navbar')   
                   
    <div id="wrapper" class="container">
      @if(Auth::check())
        <div>
          {{ Auth::user()->name }} | <a href="/auth/logout">logout</a>
        </div>  
      @else

      @endif
       
       <div class="row">
         @include('flash::message')
          <div id="primary" class="col-xs-12 col-sm-8">
              @yield('content')
          </div> <!-- END of primary -->
         
          <div id="sidebar" class="col-xs-12 col-sm-4">
              @yield('sidebar')
          </div><!-- End of Sidebar -->
      
        </div><!-- END of row -->
      
       
    </div><!-- END of container -->

  
  <body>
</html>