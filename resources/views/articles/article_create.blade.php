@extends('app')

@section('content')

  <h1>{{ $title }}</h1>

  {!! Form::model($article = new App\Article, ['url'=> 'articles']) !!}

  @include('articles._form', ['submitButtonText' => 'Add New Article'])

  {!! Form::close() !!}

@stop