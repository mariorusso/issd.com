@extends('app')

@section('content')

  
  <h1>{{ $title }}</h1>

 
      @foreach($articles as $article)

        <article>
          <a href="/articles/{{$article['id']}}"><h2>{{$article['title']}}</h2></a>
          <small>{{ $article['published_at'] }}</small>

        </article>
        @endforeach
     
@stop('content')
  
@section('sidebar')
      @include ('articles.article_sidebar')
@stop('sidebar')