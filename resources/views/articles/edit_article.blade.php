@extends('app')

@section('content')

  <h2>{{ $article->title }}</h2>

  {!! Form::model($article, ['url'=> 'articles/' . $article->id, 'method' => 'patch']) !!}

  @include('articles._form', ['submitButtonText' => 'Update Article'])

  {!! Form::close() !!}

@stop