@include('errors.list')

    <div class="form-group">
      {!! Form::label('Title:') !!}
      {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('Body:') !!}
      {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('published_at', 'Publish on (yyyy-mm-dd):') !!}
      {!! Form::text('published_at', $article->published_at, ['class' => 'form-control']) !!}
      
    </div>

    <div class="form-group">
      {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
    </div>
