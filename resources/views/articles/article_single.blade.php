@extends('app')

@section('content')

  <h1>{{ $title }}</h1>
  <hr />

    <article>
      <h2>{{ $article->title }}</h2>
      <small>{{ $article->published_at }}</small>
      <p> {!! $article->body !!}</p>
      
    </article>



@stop
