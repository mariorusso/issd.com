@if(count($errors->all() ))
  <div class="alert-danger col-xs-12">
    <ul>
      @foreach($errors->all() as $error)
        <li class="alert-danger">{{ $error }}</li>
      @endforeach

    </ul>
  </div>  
@endif