<!DOCTYPE html>

<html>
  <head>
      <title>Login Register</title>
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  </head>

  <body>
    
    <div class="container">
      
       <div class="row">
         
          <div id="primary" class="col-xs-12 col-sm-8">
              @yield('content')
          </div> <!-- END of primary -->
         
          <div id="sidebar" class="col-xs-12 col-sm-4">
              @yield('sidebar')
          </div><!-- End of Sidebar -->
      
        </div><!-- END of row -->
      
       
    </div><!-- END of container -->

  
  <body>
</html>